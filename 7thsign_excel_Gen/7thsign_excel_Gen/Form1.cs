﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel; 
namespace _7thsign_excel_Gen
{
    public partial class ExcelGenerator : Form
    {
        public ExcelGenerator()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            //myStream.
                            // Insert code to read the stream here.
                            textBox1.Text = openFileDialog1.FileName.ToString();
                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void Generate_excel_Click(object sender, EventArgs e)
        {
            //////////////////////////////////////////////
                String line;
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(textBox1.Text);
            ////////////////////////////////////////////////
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed!!");
                return;
            }

            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            ///********
            //Continue to read until you reach end of file
            //Read the first line of text
            line = sr.ReadLine();
            int x = 1, y = 1;
            String pure;
            while (line != null)
            {
                //write the lie to console window
                Console.WriteLine(line);
                if (line.Contains("level_Playing_started_at"))
                { 
                    x = x + 1;
                    y = 1;
                }
                pure = line;
                while (!pure.StartsWith(":"))
                {
                   pure =  pure.Remove(0, 1);
                }
                pure = pure.Remove(0, 1);
                xlWorkSheet.Cells[x, y] = pure;
               
                //Read the next line
                line = sr.ReadLine();
                y++;
            }
            //close the file
            sr.Close();
            Console.ReadLine();
            ///********
            string excelfielname = "d:\\7t\\7thsign.xls";
            excelfielname = textBox1.Text;
            //excelfielname.Remove(excelfielname.Length-3,3);
            excelfielname= excelfielname.Remove(excelfielname.Length-3,3);
            excelfielname = excelfielname + "xls";

            xlWorkBook.SaveAs(excelfielname, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
            MessageBox.Show("Excel file created , you can find the file " + excelfielname);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
